﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {
    private Character fighter;
    public int playerNumber = 1;
    private Dictionary<string, string> button = new Dictionary<string, string>();

    //pause delegate
    public Game.PauseDelegate IsPaused;

	// Use this for initialization
	void Start () {
        AddControl("X");
        AddControl("Y");
        AddControl("Jump");
        AddControl("Modify");
        AddControl("Attack");
	}
	
	// Update is called once per frame
	void Update () {
        if (!IsPaused())
        {
            //poll virtual input for player
            float inputX = Input.GetAxis(button["X"]);
            float inputY = Input.GetAxis(button["Y"]);
            bool inputJump = Input.GetButtonDown(button["Jump"]);
            bool inputJumpRelease = Input.GetButtonUp(button["Jump"]);
            bool inputMod = Input.GetButton(button["Modify"]);
            bool inputModRelease = Input.GetButtonUp(button["Modify"]);
            bool inputAttack = Input.GetButtonDown(button["Attack"]);

            //resolve movement
            if (inputModRelease)
            {
                fighter.EndDash();
                fighter.StopCharge();
            }

            if (inputX != 0)
            {
                if (inputMod)
                {
                    fighter.Dash(inputX);
                }
                else
                {
                    fighter.Walk(inputX);
                }
                fighter.Pivot(inputX);
            }
            else
            {
                fighter.ResetToNeutral();
                fighter.EndDash();
            }

            

            //resolve jumping
            if (inputJump)
            {
                fighter.Jump();
            }

            if (inputJumpRelease)
            {
                fighter.EndJump();
            }

            //resolve attacking
            if (inputAttack)
            {
                if (inputMod)
                {
                    fighter.ChargeAttack(inputX, inputY);
                }
                else
                {
                    fighter.StartAttack(inputX, inputY);
                }
            }
            //resolve shielding
        }
	}

    public void Posess (Character newChar)
    {
        fighter = newChar;
    }
    
    private void AddControl(string i)
    {
        string append = (playerNumber == 2) ? "2" : "";
        button.Add(i, i + append);
    }


}
