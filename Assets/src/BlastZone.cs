﻿using UnityEngine;
using System.Collections;

public class BlastZone : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        Character fighter = other.gameObject.GetComponent<Character>();
        if (fighter)
        {
            fighter.KO();
        }
    }
}
