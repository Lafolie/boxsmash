﻿using UnityEngine;
using System.Collections;

public class Sky : MonoBehaviour {
    private Material clouds;
    public float speed = 0.15f;
	// Use this for initialization
	void Start () {
        clouds = gameObject.renderer.material;
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 offset = clouds.GetTextureOffset("_MainTex");
        offset -= Vector2.right * Time.deltaTime * speed;
        clouds.SetTextureOffset("_MainTex", offset);
	}
}
