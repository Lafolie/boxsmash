﻿using UnityEngine;
using System.Collections;

public class Hitbox : MonoBehaviour {
    public Vector3 direction;
    public float knockback;
    public int damage;
    public float life;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        life -= Time.deltaTime;
        if (life <= 0)
        {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        Character fighter = other.gameObject.GetComponent<Character>();
        if (fighter)
        {
            fighter.TakeDamage(damage, direction, knockback);
            Physics.IgnoreCollision(collider, other.collider);
        }
    }
}
