﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    int percent = 0;
    int stocks = 3;
    float accel = 800f;
    float maxVel = 80f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //poll virtual input
        float inputX = Input.GetAxis("X");
        float inputY = Input.GetAxis("Y");
        bool inputJump = Input.GetButtonDown("Jump");

        float jump = (inputJump) ? 50f : 0f;
        float moveX = accel * inputX * Time.deltaTime;

        //manually set velocity
        Vector3 vel = new Vector3(moveX, jump, 0f);
        vel += rigidbody.velocity;
        vel.x = Mathf.Clamp(vel.x, -maxVel, maxVel); //keep within sensible bounds
        if (inputX == 0)
        {
            vel.x = Mathf.Lerp(vel.x, 0, 5f * Time.deltaTime);
        }

        rigidbody.velocity = vel;
        print(vel);
	}

    void OnGUI()
    {
        GUI.Box(new Rect (Screen.width / 4 - 25, Screen.height - 50, 50, 50), percent + "%\nPlayer");
    }
}
