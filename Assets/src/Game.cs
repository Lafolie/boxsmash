﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {
    //game rules
    int playerCount = 2;
    public Character[] fighters = new Character[4];
    public PlayerController[] controllers = new PlayerController[4];
    int[] stock = new int[4]; //max of 4 players
    private bool isLive = false;
    private bool hasEnded = false;
    private float countdownTimer;
    private string displayMsg = "";

    //pause stuff
    private bool isPaused = true;
    public delegate bool PauseDelegate();
    public bool GetPaused() 
    {
        return isPaused;
    }

    //ui stuff
    public GUIStyle infoStyle;
    public GUIStyle infoShadowStyle;
    public GUIStyle percentStyle;
    public GUIStyle percentShadowStyle;
    public GUIStyle displayMsgStyle;

	// Use this for initialization
	void Start () {
        //set up each player
        for (int i = 0; i <= playerCount - 1; i++)
        {
            //to-do: generate controllers based on players
            
            //pass pause delegate to controllers
            controllers[i].IsPaused = GetPaused;

            //assign pawns to controllers
            controllers[i].Posess(fighters[i]);

            //get stock rules from player prefs? (only if there is a character selection menu)
            stock[i] = 3;
        }

        // begin  the countdown!
        countdownTimer = 4f; // add additional second so that go is displayed for a second
	}
	
	// Update is called once per frame
	void Update () {
        //check countdown
        countdownTimer = Mathf.Max(0, countdownTimer - Time.deltaTime);

        if (countdownTimer > 0)
        {
            if (hasEnded)
            {
                displayMsg = "GAME!";
            }
            else if(countdownTimer > 1)
            {
                displayMsg = Mathf.CeilToInt(countdownTimer - 1).ToString();
            }
            else
            {
                displayMsg = "GO!";
                if (!isLive)
                {
                    isLive = true;
                    isPaused = false;
                }
            }
        }
        else
        {
            if (hasEnded)
            {
                Application.LoadLevel(0);
            }
            else if (isPaused)
            {
                displayMsg = "PAUSE";
            }
            else
            {
                displayMsg = "";
            }
        }

        //check for KOs
        for (int i = 0; i <= playerCount - 1; i++)
        {
            Character fighter = fighters[i];
            if (fighter.currentState == Character.State.KO && !hasEnded)
            {
                stock[i]--;

                if (stock[i] == 0)
                {
                    hasEnded = true;
                    isLive = false;
                    countdownTimer = 4;
                    ToggleFighterPaused(true);
                    continue;
                }

                // spawn new character if stocks remain
                Character newFighter = (Character)Character.Instantiate(fighter, Vector3.up * 25, fighter.transform.rotation);
                Destroy(fighter.gameObject);
                fighters[i] = newFighter;
                controllers[i].Posess(newFighter);
                

                
            }
        }

        //check Pause
        bool inputStart = Input.GetButtonDown("Start");
        ToggleFighterPaused(inputStart && isLive);

	}

    private void ToggleFighterPaused(bool inputStart)
    {
        if (inputStart)
        {
            isPaused = !isPaused;
            for (int i = 0; i <= playerCount - 1; i++)
            {
                fighters[i].ToggleFrozen();
            }
        }
    }

    void OnGUI()
    {
        // draw fighter info
        for (int i = 0; i <= playerCount-1; i++)
        {
            DrawFighterInfo(i);
        }
        
        //draw display message if appropriate
        if (displayMsg != "")
        {
            displayMsgStyle.normal.textColor = new Vector4(0, 0, 0, 255);
            GUI.Label(new Rect(Screen.width * 0.5f + 2, Screen.height * 0.25f + 2, 50, 50), displayMsg, displayMsgStyle);
            displayMsgStyle.normal.textColor = new Vector4(255, 255, 255, 255);
            GUI.Label(new Rect(Screen.width * 0.5f, Screen.height * 0.25f, 50, 50), displayMsg, displayMsgStyle);
        }

    }

    private void DrawFighterInfo(int fighterNum)
    {
        Character fighter = fighters[fighterNum];
        float gb = 1 - Mathf.Min(fighter.percent * 0.005f, 1f);
        Vector4 newColor = new Vector4(255f, gb, gb, 255f);
        percentStyle.normal.textColor = newColor;

        float xOffset = (Screen.width / 4) * (fighterNum + 1);
        GUI.Label(new Rect(xOffset, Screen.height - 88, 50, 50), fighter.percent.ToString() + "%", percentShadowStyle);
        GUI.Label(new Rect(xOffset - 2, Screen.height - 90, 50, 50), fighter.percent.ToString() + "%", percentStyle);
        GUI.Label(new Rect(xOffset, Screen.height - 38, 50, 50), fighter.fName, infoShadowStyle);
        GUI.Label(new Rect(xOffset - 2, Screen.height - 40, 50, 50), fighter.fName, infoStyle);
        for (int i = 0; i <= stock[fighterNum] - 1; i++)
        {
            GUI.Box(new Rect(xOffset + i * 16, Screen.height - 106, 16, 16), "");
        }
    }
}
