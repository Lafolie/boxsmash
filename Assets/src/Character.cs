﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {
    //game stuff
    public int percent { get; set;}
    public enum State
    {
        Neutral,
        Walking,
        Dashing,
        Attacking,
        Charging,
        Hitstun,
        Stop,
        Shielding,
        KO
    }
    private State m_currentState;
    public State currentState
    {
        get
        {
            return m_currentState;
        }
        set
        {
            if (value != m_currentState)
            {
                //cooldown = (value != State.Neutral) ? 0.05f : cooldown;
                m_currentState = value;

            }
        }
    }

    //fighter stats
    public string fName = "DevBox";
    float walkSpeed = 45f;
    float dashSpeed = 65f;
    float jumpTime = 0.5f;
    float jumpHeight = 80f;
    int maxJumps = 3;

    private float jabCool = 0.4f;
    private float jabSpeed = 10;
    private float smashCool = 0.25f;
    private float smashSpeed = 0.75f;
    private float uppercutCool = 0.25f;
    private float uppercutSpeed = 0.5f;
    private float bairCool = 0.4f;
    private float bairSpeed = 10;

    //input stats
    float cooldown = 0f;
    float jumpTimer = 2f;
    int jumps = 0;
    float chargeTimer;

    // Attack stuff
    private delegate void AttackDelegate();
    private AttackDelegate chargedAttack;
    private AttackDelegate currentAttackAnim;
    public GameObject hitboxPrefab;
    private float attackTime = 0;

    //actor stats
    public float targetFacing = 0;
    public bool onGround;
    private RigidbodyConstraints physicsConstraints;
    private Vector3 jumpVelocity;
    private float lowExtent;

    // anim stuff
    private Transform body;
    private Transform rightArm;
    private Transform leftArm;
    private Vector3 rightArmPoint;
    private Vector3 leftArmPoint;

	// Use this for initialization
	void Start () {
        currentState = State.Neutral;
        percent = 0;
        lowExtent = collider.bounds.extents.y + 0.1f;
        physicsConstraints = rigidbody.constraints;

        //find children we want to play with
        foreach (Transform child in GetComponentsInChildren<Transform>())
        {
            switch (child.name)
            {
                case "Body":
                    body = child;
                    break;
                case "RightArm":
                    rightArm = child;
                    rightArmPoint = child.localPosition;
                    break;
                case "LeftArm":
                    leftArm = child;
                    leftArmPoint = child.localPosition;
                    break;
                default:
                    continue;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
        // inc/dec timers
        cooldown -= Time.deltaTime;
        jumpTimer += Time.deltaTime;

        // currentState = (cooldown <= 0) ? State.Neutral : currentState;
        
        // movement stuff
        onGround = Physics.Raycast(transform.position, -Vector3.up, lowExtent);
        if (onGround)
        {
            jumps = 0;
        }

        jumpVelocity = Vector3.Lerp(Vector3.up * jumpHeight, Vector3.zero, jumpTimer);
        if (jumpTimer <= jumpTime)
        {
            rigidbody.MovePosition(rigidbody.position + jumpVelocity * Time.deltaTime);
            rigidbody.useGravity = (jumpTime - jumpTimer <= 0.075) ? true : false;
        }
        else
        {
            rigidbody.useGravity = true;
        }

        Vector3 rot = transform.rotation.eulerAngles;
        Vector3 newRot = new Vector3(rot.x, targetFacing, rot.z);
        transform.rotation = Quaternion.Euler(newRot);

        //Vector3 rot = Vector3.up * 360 * Time.deltaTime;
        //if (targetFacing == 0)
        //{
        //    rot = -rot;
        //}
        //transform.Rotate(rot);

        //float newY = Mathf.Clamp(transform.rotation.eulerAngles.y, rot.y, 180);
        //rot = transform.rotation.eulerAngles;
        //rot = new Vector3(rot.x, newY, rot.z);
        //transform.rotation = Quaternion.Euler(rot);

        

        //state stuff
        Vector3 newPoint;
        int direction = (targetFacing == 0) ? 1 : -1;
        switch (currentState)
        {
            case State.Neutral:
                body.rotation = Quaternion.Euler(Vector3.zero);
                newPoint = rightArmPoint + Vector3.up * (0.05f * Mathf.Sin(Time.time * 4));
                rightArm.localPosition = newPoint;
                newPoint = leftArmPoint + Vector3.up * (0.05f * Mathf.Sin(Time.time * 4));
                leftArm.localPosition = newPoint;
                break;
            case State.Walking:
                body.rotation = Quaternion.Euler(Vector3.back * 3f * direction);
                newPoint = rightArmPoint + Vector3.right * (0.3f * Mathf.Sin(Time.time * 8));
                rightArm.localPosition = newPoint;
                newPoint = leftArmPoint + -Vector3.right * (0.3f * Mathf.Sin(Time.time * 8));
                leftArm.localPosition = newPoint;
                break;
            case State.Dashing:
                body.rotation = Quaternion.Euler(Vector3.back * 10 * direction);
                newPoint = rightArmPoint + Vector3.right * (0.4f * Mathf.Sin(Time.time * 12));
                rightArm.localPosition = newPoint;
                newPoint = leftArmPoint + -Vector3.right * (0.4f * Mathf.Sin(Time.time * 12));
                leftArm.localPosition = newPoint;
                break;
            case State.Stop:
                body.rotation = Quaternion.Euler(Vector3.zero);
                if (cooldown <= 0)
                {
                    currentState = State.Neutral;
                }
                break;
            case State.Attacking:
                currentAttackAnim();
                if (cooldown <= 0)
                {
                    currentState = State.Neutral;
                    leftArm.localRotation = Quaternion.Euler(Vector3.zero);
                    rightArm.localRotation = Quaternion.Euler(Vector3.zero);
                }
                break;
            case State.Charging:
                chargeTimer += Time.deltaTime;
                //animate
                float chargeCoef = 3 - cooldown;
                body.rotation = Quaternion.Euler(Vector3.back * 7 * direction);
                newPoint = rightArmPoint + Vector3.up * (0.03f * chargeCoef * Mathf.Sin(Time.time * 40 * chargeCoef));
                rightArm.localPosition = newPoint;
                newPoint = leftArmPoint + -Vector3.up * (0.03f * chargeCoef * Mathf.Sin(Time.time * 40 * chargeCoef));
                leftArm.localPosition = newPoint;

                if (cooldown <= 0)
                {
                    currentState = State.Attacking;
                    attackTime = Time.time;
                    chargedAttack();
                }
                break;
            case State.Hitstun:
                if (cooldown <= 0)
                {
                    currentState = State.Neutral;
                }
                break;
            default:
                break;
        }
	}

    // DAMAGE METHODS
    public void KO()
    {
        body.rotation = Quaternion.Euler(Vector3.zero);
        transform.rotation = Quaternion.Euler(Vector3.zero);
        currentState = State.KO;
        cooldown = 999;
    }

    public void TakeDamage(int damage, Vector3 direction, float knockback)
    {
        rigidbody.velocity = Vector3.zero;
        direction += Vector3.up * 0.25f;
        percent += damage;
        knockback *= Mathf.Max(percent * 0.01f * knockback, 1);
        rigidbody.AddForce(direction * knockback * 100);
        currentState = State.Hitstun;
        cooldown = Mathf.Max(0.015f * knockback, 0.25f);
        float direction2 = (rigidbody.velocity.x > 0) ? 1 : -1;
        body.rotation = Quaternion.Euler(Vector3.back * 15f * direction.x);
    }

    // MOVEMENT METHODS
    public void ResetToNeutral()
    {
        if(cooldown <= 0 && currentState == State.Walking){
            currentState = State.Neutral;
        }
    }
    public void Walk(float direction)
    {
        if ((cooldown <= 0 && onGround || !onGround) && !(currentState == State.Stop || currentState == State.Dashing) || currentState == State.Walking)
        {
            Vector3 newPos = new Vector3(direction * walkSpeed, 0, 0);
            rigidbody.MovePosition(rigidbody.position + newPos * Time.deltaTime);
            if (onGround)
            {
                currentState = State.Walking;
                cooldown = 0;
            }

            float keepY = rigidbody.velocity.y;
            float newX = Mathf.Lerp(rigidbody.velocity.x, 0, Time.deltaTime * 1.25f);
            print(newX);
            rigidbody.velocity = new Vector3(newX, keepY, 0);
            
        }
    }

    public void Dash(float direction)
    {
        if (cooldown <= 0 && onGround || currentState == State.Dashing)
        {
            Vector3 newPos = new Vector3(direction * dashSpeed, 0, 0);
            rigidbody.MovePosition(rigidbody.position + newPos * Time.deltaTime);
            currentState = State.Dashing;
            float keepY = rigidbody.velocity.y;
            float newX = Mathf.Lerp(rigidbody.velocity.x, 0, Time.deltaTime);
            //newX = (newX < )
            rigidbody.velocity = new Vector3(newX, keepY, 0);
        }
        else
        {
            Walk(direction);
        }
    }

    public void EndDash()
    {
        if (currentState == State.Dashing && onGround)
        {
            print("halt");
            currentState = State.Stop;
            cooldown = 0.2f;
        }
        else if(currentState == State.Dashing && !onGround)
        {
            currentState = State.Walking;
            cooldown = 0;
        }
    }

    public void Jump()
    {
        int newJumps = (!onGround && jumps == 0) ? 2 : jumps + 1;
        if (cooldown <= 0 && newJumps <= maxJumps)
        {
            jumps = newJumps;
            jumpTimer = 0;

            //remove previous build-up of gravity velocity
            Vector3 hax = rigidbody.velocity;
            hax.y = 0;
            rigidbody.velocity = hax;
        }
    }

    public void EndJump()
    {
        jumpTimer = Mathf.Max(jumpTimer, 0.4f);
    }

    // ATTACKING METHODS
    public void StartAttack(float inputX, float inputY)
    {
        if (cooldown <= 0 && (currentState == State.Walking || currentState == State.Neutral))
        {
            if (inputY > 0)
            {
                Uppercut();
            }
            else if (inputY < 0 && !onGround)
            {
                Dair();
            }
            else if (inputX > 0)
            {
                if (!onGround && targetFacing == 180)
                {
                    Bair();
                }
                else
                {
                    targetFacing = 0;
                    Jab();
                }
                
            }
            else if (inputX < 0)
            {
                if (!onGround && targetFacing == 0)
                {
                    Bair();
                }
                else
                {
                    targetFacing = 180;
                    Jab();
                }
            }
            else
            {
                Jab();
            }
            currentState = State.Attacking;
            attackTime = Time.time;
        }
    }

    public void ChargeAttack(float inputX, float inputY)
    {
        if (cooldown <= 0 && (currentState == State.Walking || currentState == State.Neutral) && onGround)
        {
            if (inputY > 0)
            {
                chargedAttack = UpSmash;
            }
            else if (inputX > 0)
            {
                targetFacing = 0;
                chargedAttack = Smash;
            }
            else if (inputX < 0)
            {
                targetFacing = 180;
                chargedAttack = Smash;
            }
            else
            {
                chargedAttack = Smash;
            }
            currentState = State.Charging;
            cooldown = 2;
            chargeTimer = 0;
        }
    }

    public void StopCharge()
    {
        if (cooldown > 0 && currentState == State.Charging)
        {
            cooldown = 0.075f;
        }
    }

    private GameObject newHit(Transform arm)
    {
        GameObject hitbox = (GameObject)Instantiate(hitboxPrefab, arm.position, Quaternion.Euler(Vector3.zero));
        hitbox.transform.parent = arm;
        Physics.IgnoreCollision(collider, hitbox.collider);
        return hitbox;
    }

    private void Jab()
    {
        cooldown = jabCool;
        GameObject hbox = newHit(rightArm);
        Hitbox stats = hbox.GetComponent<Hitbox>();
        stats.direction = hbox.transform.parent.transform.right;
        stats.damage = 3;
        stats.knockback = 4;
        stats.life = jabCool;
        currentAttackAnim = JabAnimation;
    }

    private void JabAnimation()
    {
        int direction = (targetFacing == 0) ? 1 : -1;
        //Vector3 offset = new Vector3(rightArmPoint.x, rightArm.position.y, rightArm.position.z) + Vector3.right * Mathf.Sin(Time.time - attackTime);
        //offset *= direction;

        //Vector3 offset = Vector3.Lerp(rightArmPoint, rightArmPoint + direction * 2, (jabCool - cooldown) / jabCool);

        Vector3 newPoint = rightArmPoint + Vector3.right * (0.5f * (Mathf.Sin((Time.time - attackTime) * jabSpeed) + 1));
        rightArm.localPosition = newPoint;
    }

    private void Bair()
    {
        cooldown = bairCool;
        GameObject hbox = newHit(rightArm);
        Hitbox stats = hbox.GetComponent<Hitbox>();
        stats.direction = -hbox.transform.parent.transform.right;
        stats.damage = 8;
        stats.knockback = 8;
        stats.life = bairCool;
        currentAttackAnim = BairAnimation;
    }

    private void BairAnimation()
    {
        int direction = (targetFacing == 0) ? -1 : 1;
        rightArm.localPosition = rightArmPoint + transform.right * 1.25f * direction;
    }

    private void Dair()
    {
        EndJump();
        cooldown = bairCool;
        GameObject hbox = newHit(rightArm);
        Hitbox stats = hbox.GetComponent<Hitbox>();
        stats.direction = Vector3.down;
        stats.damage = 12;
        stats.knockback = 4;
        stats.life = bairCool;
        currentAttackAnim = DairAnimation;
    }

    private void DairAnimation()
    {
        rightArm.localPosition = rightArmPoint + Vector3.down * 0.95f;
        leftArm.localPosition = leftArmPoint + Vector3.down * 0.95f;
    }

    private void Uppercut()
    {
        cooldown = uppercutCool;
        GameObject hbox = newHit(leftArm);
        Hitbox stats = hbox.GetComponent<Hitbox>();
        hbox.transform.localScale *= 1.5f;
        stats.direction = new Vector3(transform.right.x * 0.25f, 8, 0);
        stats.damage = 5;
        stats.knockback = 2.5f;
        stats.life = uppercutCool;
        currentAttackAnim = UppercutAnimation;
        jumpTimer = 0.4f;
    }

    private void UppercutAnimation()
    {
        leftArm.localRotation = Quaternion.Euler(Vector3.forward * 90);
        float y = 0.5f + uppercutCool - (cooldown / uppercutCool) * uppercutSpeed;
        leftArm.localPosition = leftArmPoint + (Vector3.up * y) + (Vector3.right * 0.25f);
    }

    private void Smash()
    {
        cooldown = smashCool;
        GameObject hbox = newHit(rightArm);
        hbox.transform.localScale *= 2.5f;
        Hitbox stats = hbox.GetComponent<Hitbox>();
        stats.direction = hbox.transform.parent.transform.right + Vector3.up;
        stats.damage = 13 + Mathf.CeilToInt(chargeTimer * 11);
        stats.knockback = 5 + Mathf.FloorToInt(chargeTimer * 0.75f);
        stats.life = smashCool;
        currentAttackAnim = SmashAnimation;
    }


    private void SmashAnimation()
    {
        float x = (cooldown / smashCool) * smashSpeed;
        rightArm.localPosition = rightArmPoint + Vector3.right * x;
        leftArm.localPosition = leftArmPoint + Vector3.right * x;
    }

    private void UpSmash()
    {
        cooldown = smashCool;
        GameObject hbox = newHit(rightArm);
        hbox.transform.localScale *= 2f;
        Hitbox stats = hbox.GetComponent<Hitbox>();
        stats.direction = Vector3.up * 12;
        stats.damage = 15 + Mathf.CeilToInt(chargeTimer * 11);
        stats.knockback = 2 + Mathf.FloorToInt(chargeTimer * 0.95f);
        stats.life = smashCool;
        currentAttackAnim = UpSmashAnimation;
    }


    private void UpSmashAnimation()
    {
        leftArm.localRotation = Quaternion.Euler(Vector3.forward * 90);
        rightArm.localRotation = Quaternion.Euler(Vector3.forward * 90);
        float y = (cooldown / uppercutCool) * smashSpeed;
        leftArm.localPosition = leftArmPoint + (Vector3.up * y);
        rightArm.localPosition = rightArmPoint + (Vector3.up * y);
    }

    //misc methods
    public void ToggleFrozen()
    {
        enabled = !enabled;
        if (enabled)
        {
            rigidbody.constraints = physicsConstraints;
        }
        else
        {
            rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }
    }

    public void Pivot(float input)
    {
        float newFacing = targetFacing;
        if (input < 0 && cooldown <= 0 && onGround)
        {
            newFacing = 180;
        }
        else if (cooldown <= 0 && onGround)
        {
            newFacing = 0;
        }
        targetFacing = newFacing;
    }
}
